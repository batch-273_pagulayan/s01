// 1. How do you create arrays in JS?
// answer: let array = [1,2,3,4,5];

// 2. How do you access the first character of an array?
// answer: let firstElement = array[0]

// 3. How do you access the last character of an array?
// answer: let lastElement = array[array.length -1]

// 4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array
// answer: indexOf()

// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
// answer: forEach()

// 6. What array method creates a new array with elements obtained from a user-defined function?
// answer: map()

// 7. What array method checks if all its elements satisfy a given condition?
// answer: every()

// 8. What array method checks if at least one of its elements satisfies a given condition?
// anser: some()

// 9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
// answer: False

// 10.True or False: array.slice() copies elements from original array and returns them as a new array.
// answer: True

// FUNCTION CODING

// 1. Create a function named addToEnd that will add a passed in string to
// the end of a passed in array. If element to be added is not a string,
// return the string "error - can only add strings to an array". Otherwise,
// return the updated array. Use the students array and the string "Ryan"
// as arguments when testing.

let students = ["John", "Joe", "Jane", "Jessie"];

const addToEnd = (element) => {
    if(typeof element === 'string'){
        students.push(element)
        console.log(students)
    } else {
        console.log("error - can only add strings to an array")
    }
}
addToEnd(element="Ryan");

// 2. Create a function named addToStart that will add a passed in string to
// the start of a passed in array. If element to be added is not a string, return
// the string "error - can only add strings to an array". Otherwise, return the
// updated array. Use the students array and the string "Tess" as arguments
// when testing.

const addToStart = (element) => {
    if(typeof element === 'string'){
        students.unshift(element)
        console.log(students)
    } else {
        console.log("error - can only add strings to an array")
    }
}
addToStart(element="Tess");


// 3. Create a function named elementChecker that will check a passed in
// array if at least one of its elements has the same value as a passed in
// argument. If array is empty, return the message "error - passed in array is
// empty". Otherwise, return a boolean value depending on the result of the
// check. Use the students array and the string "Jane" as arguments when
// testing.

const elementChecker = (element) => {
    if (element.length === 0) { 
        console.log("error - passed in array is empty")
    } else {
        console.log(students.includes(element))
    }
}
elementChecker(element="Jane")

// 4. Create a function named checkAllStringsEnding that will accept a passed in array and
// a character. The function will do the ff:
// ● if array is empty, return "error - array must NOT be empty"
// ● if at least one array element is NOT a string, return "error - all array elements must
// be strings"
// ● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
// data type string"
// ● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
// a single character"
// ● if every element in the array ends in the passed in character, return true. Otherwise
// return false.
// Use the students array and the character "e" as arguments when testing.

const checkAllStringsEnding = (array, arg) => {
    if(array.length ===0){
        console.log("error - array must NOT be empty")
    } else if(!array.every((el) => typeof el === "string")){
        console.log("error - all array elements must be strings")
    } else if(typeof arg !== "string") {
        console.log("error - 2nd argument must be of data type string")
    } else if(arg.length !==1){
        console.log("error - 2nd argument must be a single character")
    } else {
        console.log(array.every((el) => el.endsWith(arg)))
    }
}

checkAllStringsEnding(array = ["John", "Joe", "Jane", "Jessie"], arg="e")

// 5. Create a function named stringLengthSorter that will take in an array of
// strings as its argument and sort its elements in an ascending order based
// on their lengths. If at least one element is not a string, return "error - all
// array elements must be strings". Otherwise, return the sorted array. Use
// the students array to test.

const stringLengthSorter = (array) => {
    const arrOfStrings = array.every(elem => typeof elem === 'string');
    if (!arrOfStrings) {
      console.log("error - all array elements must be strings") 
    }

    const sortedArr = array.sort((a, b) => a.length - b.length);

    if(arrOfStrings){
        console.log(sortedArr)
    }
}
stringLengthSorter(array=["John", 1, "Jane", "Jessie"])


// 6. Create a function named startsWithCounter that will take in an array of strings and a
// single character. The function will do the ff:
// ● if array is empty, return "error - array must NOT be empty"
// ● if at least one array element is NOT a string, return "error - all array elements must
// be strings"
// ● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
// data type string"
// ● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
// a single character"
// ● return the number of elements in the array that start with the character argument,
// must be case-insensitive
// Use the students array and the character "J" as arguments when testing.

const startsWithCounter = (array, char) => {

    if (array.length === 0){
        console.log("error - array must NOT be empty")
    }

    array.forEach(arr => {
        if(typeof arr !== 'string'){
            console.log("error - all array elements must be strings")
        }
    })
         
    if (typeof char !== 'string') {
        console.log("error - 2nd argument must be of data type string")
    }

    if (char.length !== 1) {
    	console.log("error - 2nd argument must be a single character")
  	}
    
    let count = 0;
  	for (let i = 0; i < array.length; i++) {
    	if (array[i].charAt(0).toLowerCase() === char.toLowerCase()) {
      		count++;
    	}
  	}
  	console.log(count)

}

startsWithCounter(array=["John", "Joe", "Jane", "Jessie"], char = "j")

// 7. Create a function named likeFinder that will take in an array of strings and a string to
// be searched for. The function will do the ff:
// ● if array is empty, return "error - array must NOT be empty"
// ● if at least one array element is NOT a string, return "error - all array elements must
// be strings"
// ● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
// data type string"
// ● return a new array containing all elements of the array argument that have the string
// argument in it, must be case-insensitive
// Use the students array and the character "jo" as arguments when testing.



const likeFinder = (array, string) => {
    //is empty
        if (array.length === 0) {
          console.log("error - array must NOT be empty")
        }
  
    //all elements are strings
        for (let i = 0; i < array.length; i++) {
          if (typeof array[i] !== "string") {
            console.log("error - all array elements must be strings")
          }
        }
  
    //second argument is a string
        if (typeof string !== "string") {
          console.log("error - 2nd argument must be of data type string")
        }
  
    // new array
        let result = [];
  
    // Loop through the array and check if each element contains the string
        for (let i = 0; i < array.length; i++) {
          if (array[i].toLowerCase().includes(string.toLowerCase())) {
            result.push(array[i]);
          }
        }
  
        console.log(result)
  }

  likeFinder(array=["John", "Joe", "Jane", "Jessie"], string="jo")

//   8. Create a function named randomPicker that will take in an array and
// output any one of its elements at random when invoked. Pass in the
// students array as an argument when testing.

  const randomPicker = (array) => {
    if (array.length === 0) {
      console.log("error - array must NOT be empty")
    }
    const randomIndex = Math.floor(Math.random() * array.length);
    console.log(array[randomIndex]);
}

randomPicker(array=["John", "Joe", "Jane", "Jessie"])
  